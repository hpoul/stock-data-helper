# Stock data helper

Some personal helper scripts to convert stock data between banks, etc.

## Status

Currently it only can:

* parse CSV exports for transactions from hellobank.
* export those transactions to a CSV suitable for finance.google.com

## Future

A few thoughts for more features

* Somehow parse data from degiro.
* Upload aggregated data to e.g. airtable.com
* Draw some nice graphs?


import fsExtra = require('fs-extra');
import * as path from 'path';
import requestPromise = require('request-promise-native');
import * as assert from 'assert';

export type Isin = string;
export type TickerSymbol = string;

interface OpenFigiMatch {
  figi: string;
  securityType: string;
  marketSector: string;
  uniqueID: string;
  ticker: string;
  name: string;
}
interface OpenFigiMatches { data: Array<OpenFigiMatch> }
interface OpenFigiError { error: string }
interface OpenFigiResponse extends Array<OpenFigiMatches|OpenFigiError> {}

function isOpenFigiError(response: OpenFigiMatches|OpenFigiError): response is OpenFigiError {
  return (<OpenFigiError>response).error !== undefined;
}

interface CachedTickerSymbol {
  ticker: TickerSymbol;
}

/**
 * Helper class to convert isin number to ticker symbol through openfigi.com
 */
export class OpenFigi {

  constructor(
    private cacheDir: string,
    private apiKey: string|null = null
  ) {

  }

  private get cacheIsinMappingFile() { return path.join(this.cacheDir, 'openfig.mapping.cache.json'); }
  private cachedIsinMapping: Map<Isin, CachedTickerSymbol>|null = null;

  private async getCachedIsinMapping(): Promise<Map<Isin, CachedTickerSymbol>> {
    if (this.cachedIsinMapping) {
      return this.cachedIsinMapping;
    }

    await fsExtra.mkdirs(this.cacheDir);

    if (await fsExtra.exists(this.cacheIsinMappingFile)) {
      const cacheData: {entries?: Array<[Isin, CachedTickerSymbol]>} = await fsExtra.readJson(this.cacheIsinMappingFile);
      if (cacheData.entries) {
        this.cachedIsinMapping = new Map(cacheData.entries);
      } else {
        this.cachedIsinMapping = new Map();
      }
    } else {
      this.cachedIsinMapping = new Map();
    }

    return this.cachedIsinMapping;
  }

  private async saveCachedIsinMapping(): Promise<void> {
    await fsExtra.writeJson(this.cacheIsinMappingFile,
      { entries: Array.from((await this.getCachedIsinMapping()).entries())});
  }

  private async getTickerSymbolFromCache(isin: Isin): Promise<TickerSymbol|null> {
    const cache = await this.getCachedIsinMapping();
    const cached = cache.get(isin);
    if (cached) {
      return cached.ticker;
    }
    return null;
  }

  private async putTickerSymbolIntoCache(isin: Isin, cachedSymbol: CachedTickerSymbol) {
    const cache = await this.getCachedIsinMapping();
    cache.set(isin, cachedSymbol);
    await this.saveCachedIsinMapping();
  }

  public async getTickerSymbol(isin: Isin): Promise<TickerSymbol> {
    return this.getTickerSymbolFromCache(isin)
      .then((cached: TickerSymbol) => {
        return cached ? cached : this.fetchTickerSymbolInfo(isin)
      });
  }
  private async fetchTickerSymbolInfo(isin: Isin): Promise<TickerSymbol> {
    const headers = this.apiKey ? {'X-OPENFIGI-APIKEY': this.apiKey} : {};
    const response: OpenFigiResponse = await requestPromise({
      method: 'POST',
      uri: 'https://api.openfigi.com/v1/mapping',
      headers: headers,
      body: [
        {
          idType: 'ID_ISIN', idValue: isin
        }
      ],
      json: true
    });
    assert(response.length === 1, "We only expect one response.");
    const matches = response[0];
    if (isOpenFigiError(matches)) {
      throw new Error(`Error response from openFigi: ${matches.error}`)
    }

    // it seems there can be many matches for each isin, but every one contains the same ticker symbol, so just grab the first.
    // assert(matches.data.length === 1, `We match for isin, imho there can only be one ticker symbol?, got: ${JSON.stringify(matches.data)}`);
    const forCache = matches.data[0];
    await this.putTickerSymbolIntoCache(isin, forCache);

    return forCache.ticker;
  }

}

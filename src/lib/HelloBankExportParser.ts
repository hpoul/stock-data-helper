import * as fs from 'fs';
import {logger} from '../logging';
import parse = require('csv-parse');
import moment = require('moment');

interface MyDecimal {
  /**
   * Simply a string with a dort marking the decimal point. nothing else...
   */
  value: string;
}

enum TransactionType {
  BUY = 'BUY',
  SELL = 'SELL',
}

export enum Market {
  /**
   * NASDAQ
   */
  NASDAQ = 'NASDAQ',

  /**
   * The New York Stock Exchange
   */
  NYSE = 'NYSE',

  /**
   * Deutsche Börse Frankfurt Stock Exchange
   */
  FRA = 'FRA',
}

export interface StockInfo {
  name?: string;
}

export function isISIN(stock: ISIN|TickerSymbol): stock is ISIN  {
  return (<ISIN>stock).isin !== undefined;
}

export interface ISIN {
  isin: string;
}
export interface TickerSymbol {
  ticker: string;
}

export interface Transaction {
  date: Date;
  type: TransactionType;
  stock: StockInfo&(ISIN|TickerSymbol);
  count: number;
  price: MyDecimal;
  currency: 'USD'|'EUR';
  fees: MyDecimal;
  market: Market|null;
}

/**
 * Simple class to read in the CSV export of hellobank for transactions.
 */
export class HelloBankExportParser {

  constructor(private csvFilePath: string) {
  }

  private parseNumber(number: string): MyDecimal {
    return {value: number.replace(',', '.')};
  }

  private parseInt(numberString: string): number {
    return +(numberString.replace(/,.*/, ''));
  }

  private parseMarket(boerseString: string): Market|null {
    switch (boerseString) {
      case 'XNMS': return Market.NASDAQ;
      case 'XNYS': return Market.NYSE;
      case 'XETR': return Market.FRA;
      case 'XBER': // Berlin market not supported right now?!
      default:
        console.warn(`Unsupported market: ${boerseString}`);
        return null;
    }
  }

  private parseRecord(record: string[]): Transaction|null {
    const typeMapping: {[key: string]: TransactionType} = {
      'Kauf': TransactionType.BUY,
      'Verkauf': TransactionType.SELL,
      // weird stuff.. just count 'Einbuchung' as a buy for now.
      'Einbuchung': TransactionType.BUY};
    const type = typeMapping[record[2]];
    if (!type) {
      logger.debug(`unknown type ${record[2]}`);
      return null;
    }

    const transaction: Transaction = {
      date: moment(record[0], 'DD.MM.YYYY').toDate(),
      type: type,
      stock: {name: record[6], isin: record[5]},
      count: this.parseInt(record[7]),
      price: this.parseNumber(record[8]),
      currency: <'EUR'|'USD'>(record[9] === 'EUR' ? 'EUR': 'USD'),
      fees: this.parseNumber(record[12]),
      market: this.parseMarket(record[18]),
    };
    logger.debug('Parsed transaction ', transaction);
    if (transaction.count === 0) {
      logger.error('Share count is zero?!');
      return null;
    }
    return transaction;
  }

  public parse(): Promise<Transaction[]> {


    return new Promise((resolve, reject) => {
      const reader = fs.createReadStream(this.csvFilePath);

      reader
        .pipe(parse({delimiter: ';', relax_column_count: true}, (err, data: string[][]) => {
          if (err) {
            return reject(err);
          }

          const ret = <Transaction[]> data.map((record) => {
            return this.parseRecord(record);
          }).filter(v => v !== null);
          logger.debug('data: ', ret);
          resolve(ret);
        }));
    });
  }
}

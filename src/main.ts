
import {logger} from './logging';
import {HelloBankExportParser, ISIN, isISIN, Transaction, Market} from './lib/HelloBankExportParser';
import csvStringify = require('csv-stringify');
import * as fs from 'fs';
import {OpenFigi} from './lib/OpenFigi';
import * as async from 'async';
import moment = require('moment');

const convertIsinToTicker = async (openFigi: OpenFigi, transactions: Transaction[]) => {
  return new Promise<Transaction[]>((resolve, reject) => {
    // Convert t.stock.isin to t.stock.ticker
    async.map(transactions, async (t) => {
      if (isISIN(t.stock)) {
        t.stock = {ticker: await openFigi.getTickerSymbol(t.stock.isin), isin: t.stock.isin};
      }
      return t;
    }, (err, success: Transaction[]) => {
      if (err) {
        reject(err);
      }
      resolve(success);
    });
  });
};

const parser = new HelloBankExportParser('import/Datenexport_20170717_213825.csv');
(async () => {

  logger.info('Running');

  const openFigi = new OpenFigi('./cache/', process.env.OPENFIGI_API_KEY);

  const result = await convertIsinToTicker(openFigi, await parser.parse());

  logger.info("Everything was parsed:\n\n\n");
  logger.info(JSON.stringify(result));


  // Now write a CSV which can be loaded into finance.google.com

  (<any>csvStringify)(
    result,
    {
      formatters: {
        object: (obj: any) => {
          return obj.ticker || obj.value || obj;
        },
        date: (date: Date) => moment(date).format('YYYY-MM-DD'),//moment(date).format('DD/MM/YYYY'),
      }
    },
    (err: any, output: string) => {
    fs.writeFileSync('out.csv', output);
  });

})().then(() => {
  // success.. nothing to do :-)
  logger.info("All Done.");
}).catch((error) => {
  logger.error("Uncaught error during execution?!", error);
});
